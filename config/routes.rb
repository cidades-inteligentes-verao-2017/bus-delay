Rails.application.routes.draw do
  root 'bus_delay#index'
  get '/trips' => 'trips#index'
  get '/traffic_delay' => 'bus_delay#traffic_delay'
  get '/data_quality' => 'bus_delay#data_quality'
  get '/stops' => 'bus_delay#stops'
  get '/trips/:trip_id' => 'trips#show'
  get '/trips/:trip_id/snapshot' => 'snapshot#show'
  get '/trips/:trip_id/snapshot/data' => 'snapshot#show_ajax'
  get '/trips/:trip_id/bus_interval' => 'bus_interval#index'
  get '/trips/:trip_id/bus_interval/data' => 'bus_interval#data'
  get '/trips/:trip_id/bus_interval/trips' => 'bus_interval#trips'
  get '/trips/:trip_id/bus_interval/statistics' => 'bus_interval#statistics'
  get '/trips/:trip_id/freq_dispatch/data' => 'freq_dispatch#data'
  get '/summary' => 'summary#index'
  get '/summary/data' => 'summary#data'
  get '/about' => 'about#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
