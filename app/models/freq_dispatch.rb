class FreqDispatch
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  store_in collection: "logs_bustrips.freq_table"

  def self.get_freq_info(freqs_dispatch)

    freq_hourly = {}
   	freqs_dispatch.each do |freq|
	    freq_hourly = freq["hourly_by_stop"]
   	end
   	
    organized_data = []
    cont = 0
    stops = {}

   	#Calculate for each hour
  	while cont < 24
  		planned = 0
  		detected = 0
  		
  		unless freq_hourly[cont].nil?
			  stops[cont] = freq_hourly[cont]["stops"]
			  detected = FreqDispatch.get_most_freq(stops[cont])

			  stops[cont].each do |stop|
			  	stop.each do |key,value|
    				if key == "planned" && !value.nil?
    					planned = value 
    					break;
    				end
  				end
		 	  end
		 	end
		  # organized_data << [human_friendly_interval(cont), planned, detected]
      organized_data << [cont, planned, detected]
	 	  cont+=1
		end
    
    organized_data

	end

  def self.stops_data
    grouped = FreqDispatch.collection.aggregate([
      {"$unwind": {
        "path": "$hourly_by_stop",
        "preserveNullAndEmptyArrays": false
      }},
      {"$unwind": {
        "path": "$hourly_by_stop.stops",
        "preserveNullAndEmptyArrays": false
      }},
      {"$project": {
        "hourly_by_stop": true,
        "value": {
          "$cond": {
            "if": {"$eq": ["$hourly_by_stop.stops.avg_interval", Float::NAN]},
            "then": nil,
            "else": "$hourly_by_stop.stops.avg_interval"
          }
        }
      }},
      {"$group": {
        "_id": "$hourly_by_stop.stops.stop_id",
        "avg": {"$avg": "$value"},
				"sum": {"$sum": "$value"},
				"max": {"$max": "$value"},
				"min": {"$min": "$value"},
        "count": {"$sum": 1}
      }},
    ])

    return grouped
  end


  def self.get_most_freq(stops_per_hour)
  	real = Hash.new(0)
  	detected = 0

  	unless stops_per_hour.nil?
		  stops_per_hour.each do |info|
		  	info.each do |key,value|
					real[value] +=1 if key == "detected" && !value.nil?
				end
			end
		end
 		
 		max_freq = real.values.max
 		result = real.select { |k, v| v == max_freq }.keys
    result[0]
  end

  def self.make_summary(organized_data)

    fulfill = 0
    not_fulfill = 0
    no_buses = 0

    organized_data.each do |buses_per_hour|
      #buses_per_hour[1] is the number planned
      #buses_per_hour[2] is the number detected
      if (buses_per_hour[1] && buses_per_hour[2]) != 0
        if buses_per_hour[1] == buses_per_hour[2]
          fulfill +=1
        else
          not_fulfill +=1
        end
      else
        no_buses +=1
      end
    end

    info = [fulfill, not_fulfill, no_buses]

  end

  def self.human_friendly_interval(hour)
    interval_map = {
      0 => "00-01", 1 => "01-02", 2 => "02-03", 3 => "03-04",
      4 => "04-05", 5 => "05-06", 6 => "06-07", 7 => "07-08",
      8 => "08-09", 9 => "09-10", 10 => "10-11", 11 => "11-12",
      12 => "12-13", 13 => "13-14", 14 => "14-15", 15 => "15-16",
      16 => "16-17", 17 => "17-18", 18 => "18-19", 19 => "19-20",
      20 => "20-21", 21 => "21-22", 22 => "22-23", 23 => "23-00"
    }

    interval_map[hour]
  end

end
