class Stop
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  store_in collection: "stops"
end
