class Trip
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  store_in collection: "trips"
  field :route_id

  index({ route_id: 1 }, { unique: false, name: "reout_id_index" })

  def self.search(search)
    any_of({trip_id: /#{search}/i}, {headsign: /#{search}/i})
  end
end
