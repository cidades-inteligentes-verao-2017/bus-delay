class BusInterval
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  store_in collection: "bus_intervals"

  field :trip_id, type: String
  field :begin_interval, type: Float
  field :end_interval, type: Float
  field :error, type: Float
  field :current_bus, type: Hash
  field :next_bus, type: Hash

  before_save :update_error

  def self.statistics(trip_id)
    grouped = BusInterval.collection.aggregate([
      {"$match": {"trip_id": trip_id}},
      {"$group": {
        "_id": {"$hour": "$current_bus.begin_ts"},
        "stdDev": {"$stdDevPop": "$begin_interval"},
        "avg": {"$avg": "$begin_interval"}
      }},
      {"$sort": {"_id": 1}}
    ])

    organized_statistics = []
    grouped.each do |interval|
      organized_statistics <<
      [
        BusInterval.human_friendly_interval(interval["_id"]),
        interval["avg"],
        interval["stdDev"]
      ]
    end
    organized_statistics
  end

  def self.mean_traffic_delay_by_trip(travels = 50, limit = 5)
    grouped = BusInterval.collection.aggregate([
      {"$group": {
        "_id": "$trip_id",
        "avg": {"$avg": "$error"},
        "stdDev": {"$stdDevPop": "$error"},
        "count": {"$sum": 1},
      }},
      {"$sort": {"avg": -1}},
    ])

    grouped.to_a.reject{|x| x["count"] < travels}[0..limit]
  end

  def self.human_friendly_interval(hour)
    interval_map = {
      0 => "00-01", 1 => "01-02", 2 => "02-03", 3 => "03-04",
      4 => "04-05", 5 => "05-06", 6 => "06-07", 7 => "07-08",
      8 => "08-09", 9 => "09-10", 10 => "10-11", 11 => "11-12",
      12 => "12-13", 13 => "13-14", 14 => "14-15", 15 => "15-16",
      16 => "16-17", 17 => "17-18", 18 => "18-19", 19 => "19-20",
      20 => "20-21", 21 => "21-22", 22 => "22-23", 23 => "23-00"
    }

    interval_map[hour]
  end

  protected

  def update_error
    self.error = end_interval - begin_interval
  end
end
