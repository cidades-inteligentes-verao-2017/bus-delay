class BusTrip
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  store_in collection: "logs_bustrips"


  def self.to_csv()
    desired_columns = ["begin_ts", "travel_time"]
    CSV.generate(headers: true) do |csv|
      csv << desired_columns
      all.each do |trip|
        csv << trip.attributes.values_at(*desired_columns)
      end
    end
  end

  def travel_time_in_minutes
    if self.end_ts && self.begin_ts
      (self.end_ts - self.begin_ts) / 60.0
    end
  end

  scope :snapshot, ->(trip_id, timestamp) {
    where(trip_id: trip_id, :begin_ts.lte => timestamp,
                  :end_ts.gte => timestamp)
  }

  scope :valid, -> { where(:status.in => ["completed", "partial"]) }

  def self.snapshot_positions(trip_id, timestamp)
    bus_trips = BusTrip.snapshot(trip_id, timestamp).valid()

    trips = {}
    bus_trips.each do |bus_trip|
      trips[bus_trip.avl_id] = bus_trip.stops[:list]
    end

    BusTrip.reduce_stops(trips, timestamp)
  end

  def self.mean_travel_time_by_hour(trip_id)
    grouped = BusTrip.collection.aggregate([
      {"$match": {
        "trip_id": trip_id,
        "$or": [{ "status": "completed" }, { "status": "partial"}],
      }},
      {"$group": {
        "_id": {"$hour": "$begin_ts"},
        "avg": {"$avg": "$travel_time"}
      }},
      {"$sort": {"_id": 1}}
    ])

    organized_statistics = {}
    grouped.each do |interval|
      organized_statistics[interval["_id"]] = interval["avg"]
    end
    organized_statistics
  end

  def self.mean_travel_time_by_trip(limit = 5)
    grouped = BusTrip.collection.aggregate([
      {"$match": {
        "$or": [{ "status": "completed" }, { "status": "partial"}],
      }},
      {"$group": {
        "_id": "$trip_id",
        "avg": {"$avg": "$travel_time"},
        "stdDev": {"$stdDevPop": "$travel_time"},
        "count": { "$sum": 1 }
      }},
      {"$sort": {"count": -1}},
    ])

    return grouped
  end

  def self.human_friendly_interval(hour)
    interval_map = {
      0 => "00-01", 1 => "01-02", 2 => "02-03", 3 => "03-04",
      4 => "04-05", 5 => "05-06", 6 => "06-07", 7 => "07-08",
      8 => "08-09", 9 => "09-10", 10 => "10-11", 11 => "11-12",
      12 => "12-13", 13 => "13-14", 14 => "14-15", 15 => "15-16",
      16 => "16-17", 17 => "17-18", 18 => "18-19", 19 => "19-20",
      20 => "20-21", 21 => "21-22", 22 => "22-23", 23 => "23-00"
    }

    interval_map[hour]
  end

  def self.sorted_snapshot_positions(trip_id, timestamp)
    BusTrip.snapshot_positions(trip_id, timestamp).sort_by {|bus_id, body| body['stop_seq']}
  end

  def begin_interval(next_bus)
    if next_bus && next_bus.begin_ts && self.begin_ts
      (next_bus.begin_ts - self.begin_ts).abs / 60.0
    end
  end

  def end_interval(next_bus)
    if next_bus && next_bus.end_ts && self.end_ts
      (next_bus.end_ts - self.end_ts).abs / 60.0
    end
  end

  def acceptable?
    self.status == "completed" || self.status == "partial"
  end

  private

  def self.reduce_stops(trips, timestamp)
    stops = {}
    trips.each do |bus_id, trip_stops|
      stops[bus_id] = nil
      interval = nil
      trip_stops.each do |trip_stop|
        next if trip_stop.nil?
        current_interval = trip_stop[:ts_avl] ? (trip_stop[:ts_avl] - timestamp).abs : nil
        next if current_interval.nil?
        if stops[bus_id].nil?
          interval = current_interval
          stops[bus_id] = trip_stop.merge(interval: interval)
        elsif current_interval <= interval
          interval = current_interval
          stops[bus_id] = trip_stop.merge(interval: interval)
        end
      end
    end

    remove = []
    stops.each do |bus_id, trip_stops|
      stop = Stop.where(stop_id: trip_stops[:stop_id]).first
      if stop.nil?
        remove << bus_id
      else
        trip_stops.merge!(location: stop.loc)
      end
    end

    remove.each {|id| stops.delete(id)}

    stops
  end
end
