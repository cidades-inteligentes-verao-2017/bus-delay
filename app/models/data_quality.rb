class DataQuality
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  store_in collection: "data_quality"
end
