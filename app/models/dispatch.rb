class Dispatch
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  store_in collection: "dispatch_buses"
  
  field :fulfill, type: Integer
  field :not_fulfill, type: Integer
  field :no_scheduled_buses, type: Integer
  field :trip_id, type: String
  
end
