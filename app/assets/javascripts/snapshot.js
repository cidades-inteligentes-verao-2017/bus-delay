function initSnapshotMap(url, trip){
  // Initial coordinates on which to center the map when the page is loaded.
  var INIT_LAT = -23.545241799982602;
  var INIT_LNG = -46.63861848413944;
  var INIT_ZOOM = 12;

  var map;
  var markers = [];

  function get_trip(minutes, trip_url) {
    clearMarkers();
    var time = minutesTimeSpanToHMS(minutes);
    var timestamp = "2014-10-21 " + time + ' UTC'
    var url = "/trips/" + trip_url + "/snapshot/data.json"
    $.ajax({
      dataType: "json",
      url: url,
      data: {timestamp: timestamp},
    }).done(function(data) {
      count = 0;
      $.each(data, function(i, item) {
        count += 1;
        var lat = item.location.lat;
        var lng = item.location.lng;
        var myLatLng = {lat: lat, lng: lng};
        drawMarker(myLatLng, data);
      });
      $("#trip_info").html("Número de carros:" + count);
      $("#trip_info").append("<br>Horário: " + time);
    });
  }

  function minutesTimeSpanToHMS(m) {
      var h = Math.floor(m/60); //Get whole hours
      m -= h*60;
      s=0;
      return (h < 10 ? '0'+h : h)+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
  }

  function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
  }

  function drawMarker(myLatLng, data) {

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: 'stop_id:' + data.stop_id
    });

    var contentString = "Test content";

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
    markers.push(marker);

  }

  function drawTripShape(shape) {
    var shapePath = [];

    shape.forEach(function(item) {
      shapePath.push(item.loc);
    });

    var path = new google.maps.Polyline({
      path: shapePath,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });

    path.setMap(map);
  }


  function drawShape(trip_url) {
    var url = "/trips/" + trip_url;
    $.ajax({
      dataType: "json",
      url: url,
    }).done(function(data) {
      var shapePath = [];

      data.shape.forEach(function(item) {
        shapePath.push(item.loc);
      });

      var path = new google.maps.Polyline({
        path: shapePath,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
      });
      path.setMap(map);

      var start_point = data.shape[0];
      var end_point = data.shape[data.shape.length - 1];
      drawTripShape(data.shape);
      drawStartAndEndMarks(start_point, end_point);
    });
  }

  function initMap() {
    var myLatLng = {lat: INIT_LAT, lng: INIT_LNG};

    map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: INIT_ZOOM
    });
  }

  initMap();
  drawShape(trip);
  $( "#timestamp" ).mouseup(function() {
    var minutes = $('#timestamp').val();
    get_trip(minutes, trip);
  });
  $( "#timestamp" ).keyup(function() {
    var minutes = $('#timestamp').val();
    get_trip(minutes, trip);
  });
}
