function plotGeneralDashboard() {
  google.charts.setOnLoadCallback(drawChart);

  function get_traffic_delay() {
    var data = [];
    return new Promise(function(resolve,reject) {
      $.ajax({
        url: "/traffic_delay.json",
      }).done(function(raw_data) {
        $.each(raw_data, function(index, value) {
          var temp = [];
          temp[0] = value._id;
          temp[1] = Number(value.avg.toFixed(2));
          data.push(temp)
        });
        resolve(data);
      });
    });
  }

  function plot_traffic_delay_chart(raw_value) {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Trip');
    data.addColumn('number', 'Delay (min)');
    $.each(raw_value, function(index, value) {
      data.addRow([value[0], value[1]]);
    });
    var plot_data = data;
    var options = {
      title: 'Average delay due to traffic',
      hAxis: {
        title: 'Bus Trip',
        titleTextStyle: {color: '#333'},
      },
      vAxis: {
        title: 'Delay (minutes)',
        minValue: 0,
        gridlines: {count: 5}
      },
      focusTarget: 'category'
    };
    var chart = new google.visualization.ColumnChart(document.getElementById('traffic_delay_chart_div'));
    chart.draw(plot_data, options);
  }

  function get_data_quality() {
    var data = [];
    return new Promise(function(resolve,reject) {
      $.ajax({
        url: "/data_quality.json",
      }).done(function(raw_data) {
        data.push(["completed", raw_data["completed"]]);
        data.push(["partial", raw_data["partial"]]);
        data.push(["reversed", raw_data["reversed"]]);
        data.push(["error", raw_data["error"]]);
        resolve(data);
      });
    });
  }

  function plot_data_quality_chart(raw_value) {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Data Quality');
    data.addColumn('number', 'Number of Trips');
    data.addRows(raw_value);
    var plot_data = data;
    var options = {
      backgroundColor: { fill:'transparent' },
      slices: {
        0: { color: '#303F9F' },
        1: { color: '#7986CB' },
        2: { color: '#F8BBD0' },
        3: { color: '#E53935' }
      }
    };
    var chart = new google.visualization.PieChart(document.getElementById('data_quality__chart_div'));
    chart.draw(plot_data, options);
  }

  function drawChart() {
    var raw_data = get_traffic_delay();
    $( document ).ready(function() {
      raw_data.then(function(raw_value) {
        plot_traffic_delay_chart(raw_value);
      });
    });

    var data_quality = get_data_quality();
    $( document ).ready(function() {
      data_quality.then(function(raw_value) {
        plot_data_quality_chart(raw_value);
      });
    });
  }
}


function plotSummaryDispatch(trip_url) {

  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);

  function get_data() {
    var data = [];
    return new Promise(function(resolve,reject) {
      $.ajax({
        url: "/summary/data.json",
      }).done(function(raw_data) {
        $.each(raw_data, function(index, value) {
          data.push(value)
        });
        resolve(data);
      });
    });
  }


  function sortAssocObject(list) {
      var sortable = [];
      for (var key in list) {
        sortable.push([key, list[key]]);
      }

      sortable.sort(function(a, b) {
        return (a[1] < b[1] ? -1 : (a[1] > b[1] ? 1 : 0));
      });

      var orderedList = {};
      for (var i = 0; i < sortable.length; i++) {
        orderedList[sortable[i][1]] = sortable[i][0];
      }
      return orderedList;
  }


  function plot_fulfill_chart(raw_value) {

    var accomplished_data = [["Bus line", "Fulfill"]];
    var data = new google.visualization.DataTable();
    data.addColumn("string", "Bus Line");
    data.addColumn("number", "Fulfill");
    fulfill = {}

    $.each(raw_value, function(index, value) {
      todivide = (value[0] + value[1]) / 100
      if (todivide == 0)
        fulfill[value[3]] = value[0]
      else
        fulfill[value[3]] = value[0]/todivide
    });

    ordered = sortAssocObject(fulfill);

    cont = 0
    size = Object.keys(ordered).length - 1;
    while (cont < 5) {
      data.addRow([ordered[Object.keys(ordered)[size]], parseFloat(Object.keys(ordered)[size])]);
      size--;
      cont++;
    }

    console.log(accomplished_data);
    var info = google.visualization.arrayToDataTable(accomplished_data);

    var options = {
      title: "Top 5 lines that accomplished the contract",
      colors: ['#e0440e', '#e6693e', '#ec8f6e', '#f3b49f', '#f6c7b6'],
    };
    var chart = new google.visualization.ColumnChart(document.getElementById("fulfill"));
    chart.draw(data, options);
  }

  function plot_not_fulfill_chart(raw_value) {

    var accomplished_data = [["Bus line", "Not fulfill"]];
    fulfill = {}

    $.each(raw_value, function(index, value) {
      todivide = (value[0] + value[1]) / 100
      if (todivide == 0)
        fulfill[value[3]] = value[1]
      else
        fulfill[value[3]] = value[1]/todivide
    });

    ordered = sortAssocObject(fulfill);

    cont = 0

    while (cont < 5) {
      accomplished_data.push([ordered[Object.keys(ordered)[cont]], parseFloat(Object.keys(ordered)[cont])]);
      cont++;
    }

    console.log(accomplished_data);
    var data = google.visualization.arrayToDataTable(accomplished_data);

    var options = {
      title: "Top 5 lines that don't accomplished the contract",
      is3D: true,

    };

    var chart = new google.visualization.ColumnChart(document.getElementById("not_fulfill"));
    chart.draw(data, options);
  }


  function drawChart() {
    var raw_data = get_data();
    raw_data.then(function(raw_value) {
      plot_fulfill_chart(raw_value);
      plot_not_fulfill_chart(raw_value);
    });
  }
}

function createHeatMap() {
  var INIT_LAT = -23.545241799982602;
  var INIT_LNG = -46.63861848413944;
  var NEW_INIT_ZOOM = 13;

  var map;
  var markers = [];

  function drawHeatMap() {
    $.ajax({
      dataType: "json",
      url: "/stops.json",
    }).done(function(raw_data) {
      var heatMapData = []
      $.each(raw_data, function(index, value) {
        var temp = {location: new google.maps.LatLng(value[0], value[1]), weight: Math.floor(value[2])};
        heatMapData.push(temp);
      });
      var heatmap = new google.maps.visualization.HeatmapLayer({
        data: heatMapData
      });
      heatmap.set('radius', heatmap.get('radius') ? null : 20);
      heatmap.set('maxIntensity', heatmap.get('maxIntensity') ? null : 60);
      heatmap.set('opacity', heatmap.get('opacity') ? null : 0.4);
      heatmap.setMap(map);
    });
  }

  function initHeatMap() {
    var myLatLng = {lat: INIT_LAT, lng: INIT_LNG};

    map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: NEW_INIT_ZOOM
    });
  }

  initHeatMap();
  drawHeatMap();
}
