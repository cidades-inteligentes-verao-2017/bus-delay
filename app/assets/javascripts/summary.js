function plotSummaryDispatch(trip_url) {

  google.charts.setOnLoadCallback(drawChart);

  function get_data() {
    var url = "/summary/data.json";
    var data = [];
    return new Promise(function(resolve,reject) {
      $.ajax({
        url: url,
      }).done(function(raw_data) {
        $.each(raw_data, function(index, value) {
          data.push(value)
        });
        resolve(data);
      });
    });
  }


  function sortAssocObject(list) {
      var sortable = [];
      for (var key in list) {
        sortable.push([key, list[key]]);
      }

      sortable.sort(function(a, b) {
        return (a[1] < b[1] ? -1 : (a[1] > b[1] ? 1 : 0));
      });

      var orderedList = {};
      for (var i = 0; i < sortable.length; i++) {
        orderedList[sortable[i][1]] = sortable[i][0];
      }
      return orderedList;
  }


  function plot_fulfill_chart(raw_value) {

    var accomplished_data = [["Bus line", "Fulfill"]];
    var data = new google.visualization.DataTable();
    data.addColumn("string", "Bus Line");
    data.addColumn("number", "Fulfill");
    fulfill = {}

    $.each(raw_value, function(index, value) {
      todivide = (value[0] + value[1]) / 100
      if (todivide == 0)
        fulfill[value[3]] = value[0]
      else
        fulfill[value[3]] = value[0]/todivide
    });

    ordered = sortAssocObject(fulfill);

    cont = 0
    size = Object.keys(ordered).length - 1;
    while (cont < 5) {
      data.addRow([ordered[Object.keys(ordered)[size]], parseFloat(Object.keys(ordered)[size])]);
      size--;
      cont++;
    }

    console.log(accomplished_data);
    var info = google.visualization.arrayToDataTable(accomplished_data);

    var options = {
      title: "Top 5 lines that accomplished the contract",
    };
    var chart = new google.visualization.ColumnChart(document.getElementById("fulfill"));
    chart.draw(data, options);
  }

  function plot_not_fulfill_chart(raw_value) {

    var accomplished_data = [["Bus line", "Not fulfill"]];
    fulfill = {}

    $.each(raw_value, function(index, value) {
      todivide = (value[0] + value[1]) / 100.0;
      if (todivide == 0)
        fulfill[value[3]] = 100;
      else
        fulfill[value[3]] = value[1]/todivide;
    });

    ordered = sortAssocObject(fulfill);

    cont = 0

    while (cont < 5) {
      accomplished_data.push([ordered[Object.keys(ordered)[cont]], parseFloat(Object.keys(ordered)[cont])]);
      cont++;
    }

    console.log(accomplished_data);
    var data = google.visualization.arrayToDataTable(accomplished_data);

    var options = {
      title: "Top 5 lines that did not accomplish the contract",
      is3D: true,

    };

    var chart = new google.visualization.ColumnChart(document.getElementById("not_fulfill"));
    chart.draw(data, options);
  }


  function drawChart() {
    var raw_data = get_data();
    raw_data.then(function(raw_value) {
      plot_fulfill_chart(raw_value);
      plot_not_fulfill_chart(raw_value);
    });
  }
}
