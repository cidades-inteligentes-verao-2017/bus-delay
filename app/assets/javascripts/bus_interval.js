function plotBusInterval(trip_url) {
  google.charts.setOnLoadCallback(drawChart);

  function get_data() {
    var url = "/trips/" + trip_url + "/bus_interval/data.json";
    var data = [];
    return new Promise(function(resolve,reject) {
      $.ajax({
        url: url,
      }).done(function(raw_data) {
        $.each(raw_data, function(index, value) {
          value[0] = new Date(value[0]);
          value[1] = Math.floor(value[1])
          value[2] = Math.floor(value[2])
          value[3] = Math.floor(value[3])
          value[4] = Math.floor(value[4])
          data.push(value)
        });
        resolve(data);
      });
    });
  }

  function get_statistics() {
    var url = "/trips/" + trip_url + "/bus_interval/statistics.json";
    var data = [];
    return new Promise(function(resolve,reject) {
      $.ajax({
        url: url,
      }).done(function(raw_data) {
        $.each(raw_data, function(index, value) {
          value[0] = value[0];
          value[1] = Math.round(value[1]);
          value[2] = Math.round(value[2]);
          data.push(value)
        });
        resolve(data);
      });
    });
  }

  function get_bus_trip_intervals() {
    var url = "/trips/" + trip_url + "/bus_interval/trips.json";
    var data = [];
		return new Promise(function(resolve,reject) {
			$.ajax({
				url: url,
			}).done(function(raw_data) {
				$.each(raw_data, function(index, value) {
					var new_value = [];
					value.forEach(function(item){
						if(value[0] == item || item == null){
							new_value.push(item);
						}
						else{
							new_value.push(new Date(item));
						}
					});
					data.push(new_value);
				});
				resolve(data);
			});
		});
	}

	function get_freq_info() {
		var url = "/trips/" + trip_url + "/freq_dispatch/data.json";
		var data = [];
		return new Promise(function(resolve,reject) {
			$.ajax({
				url: url,
			}).done(function(raw_data) {
				$.each(raw_data, function(index, value) {
					value[0] = value[0]; //hour
					value[1] = value[1];
					value[2] = value[2];
					data.push(value)
				});
				resolve(data);
			});
		});
	}

  function plot_bus_interval_chart(raw_value) {
    var data = new google.visualization.DataTable();
    data.addColumn('datetime', 'Depature Time');
    data.addColumn('number', 'Bus Interval Impact');
    data.addColumn('number', 'Mean Bus Interval Impact');
    data.addColumn({type:'string', role:'color'});
    var mean = 0;
    var count = 0;
    $.each(raw_value, function(index, value) {
      mean += value[4];
      count += 1;
    });
    mean = mean/count;
    var interval_data = [];
    $.each(raw_value, function(index, value) {
      var color = value[4] > 0 ? "color: green" : "color: blue";
      console.log(color);
      console.log(Math.abs(value[4]));
      interval_data.push([value[0], value[4], mean, color])
    });
    data.addRows(interval_data);
    var plot_data = data;
    var options = {
      title: 'Traffic Impact on the Distance of Consecutives Buses',
      hAxis: {
        title: 'Hour of the day',
        titleTextStyle: {color: '#333'},
        gridlines: {count: 10}
      },
      vAxis: {
        title: 'Minutes',
        minValue: 0,
        gridlines: {count: 6}
      },
      series: {1: {type: 'line'}},
      focusTarget: 'category'
    };
    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
    chart.draw(plot_data, options);

  }

  function plot_travel_time_chart(raw_value) {
    var interval_data = [["begin_ts", "Travel Time", "Hourly Mean", "Mean"]];
    var mean = 0;
    var count =0;
    $.each(raw_value, function(index, value) {
      mean += value[3];
      count += 1;
    });
    mean = mean/count;
    $.each(raw_value, function(index, value) {
      interval_data.push([value[0], value[3], value[5], mean]);
    });
    var plot_data = google.visualization.arrayToDataTable(interval_data);
    var options = {
      title: 'Travel Time in a line',
      hAxis: {
        title: 'Hour of the day',
        titleTextStyle: {color: '#333'},
        gridlines: {count: 10}
      },
      vAxis: {
        title: 'Minutes',
        minValue: 0
      },
      series: {2: {type: 'line'}},
      series: {3: {type: 'line'}},
      focusTarget: 'category',
    };
    var chart = new google.visualization.LineChart(document.getElementById('travel_time_chart_div'));
    chart.draw(plot_data, options);

  }

  function plot_aggregated_statistics(raw_value) {
    var interval_data = [["Time Interval", "Average", "Positive Standard Deviation", "Negative Standard Deviation"]];
    var plot_data = new google.visualization.DataTable();
    plot_data.addColumn('string', "Time Interval");
    plot_data.addColumn('number', "Average");
    plot_data.addColumn({id:'Positive Standard Deviation', type:'number', role:'interval'});
    plot_data.addColumn({id:'Negative Standard Deviation', type:'number', role:'interval'});
    $.each(raw_value, function(index, value) {
      plot_data.addRow([value[0], value[1], value[1] + value[2], value[1] - value[2]]);
    });
    var options = {
      title: 'Average Departure Interval',
      hAxis: {
        title: 'Hour of the day',
        titleTextStyle: {color: '#333'},
        gridlines: {count: 12}
      },
      vAxis: {
        title: 'Departure Interval',
      },
      focusTarget: 'category',
    };
    var chart = new google.visualization.ColumnChart(document.getElementById('statistics_chart_div'));
    chart.draw(plot_data, options);
  }

  function plot_trips_lines(trips_data){
    var plot_data = new google.visualization.DataTable();
    plot_data.addColumn('number', 'bus');
    trips_data[0].forEach(function(item, index){
      if(item instanceof Date || item == null){
        plot_data.addColumn('datetime', "Bus " + index);
      }
    });
    plot_data.addRows(trips_data);
    var options = {
      title: 'Interval Between Buses by Bus Stops',
      hAxis: {
        title: 'Stop Sequence',
        titleTextStyle: {color: '#333'},
        gridlines: {count: 12}
      },
      vAxis: {
        title: 'Hour of the day',
        viewWindow: {
          min: new Date("2014-10-21 17:00:00 UTC"),
          max: new Date("2014-10-21 21:00:00 UTC"),
        },
        titleTextStyle: {color: '#333'},
        gridlines: {count: 12}
      },
      focusTarget: 'category',
    };
    var chart = new google.visualization.LineChart(document.getElementById('trips_chart_div'));
    chart.draw(plot_data, options);
  }

	function plot_frequency_dispatch(raw_value) {
		var dispatch_data = [["Time Interval", "Planned Dispatch", "Detected Dispatch"]];
		$.each(raw_value, function(index, value) {
			dispatch_data.push([value[0], value[1], value[2]])
		});
		var plot_data = google.visualization.arrayToDataTable(dispatch_data);
		var options = {
			title: 'Planned X Real Dispatch',
      subtitle: "Just example",
      legend: {
        text: "Exemplo de texto",
      },
			hAxis: {
				title: 'Hour of the day',
				titleTextStyle: {color: '#333'},
				gridlines: {count: 12}
			},
			vAxis: {
				title: 'Number of Buses',
				minValue: 0
			},
			focusTarget: 'category'
		};
		var chart = new google.visualization.ColumnChart(document.getElementById('freq_dispatch_chart_div'));
		chart.draw(plot_data, options);

  }


  function drawChart() {
    var raw_data = get_data();
    raw_data.then(function(raw_value) {
      plot_bus_interval_chart(raw_value);
      plot_travel_time_chart(raw_value);
    });

    var statistics_data = get_statistics();
    statistics_data.then(function(statistics_data) {
      plot_aggregated_statistics(statistics_data);
    });

    var trips_data = get_bus_trip_intervals();
    trips_data.then(function(trips_data) {
      plot_trips_lines(trips_data);
    });

		var dispatch_data = get_freq_info();
		dispatch_data.then(function(dispatch_data) {
			plot_frequency_dispatch(dispatch_data);
		});

  }
}
