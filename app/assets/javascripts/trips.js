function initTripMap(received_url) {
  // Initial coordinates on which to center the map when the page is loaded.
  var INIT_LAT = -23.545241799982602;
  var INIT_LNG = -46.63861848413944;
  var INIT_ZOOM = 11;

  var map;
  var markers = [];

  function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
  }

  function drawStops(stops) {
    stops.forEach(function(item){
      var contentString = "<h4>Stop " + item.stop_seq + "</h4>" +
      "<div>ID: " + item.stop_id + "</div>" +
      "<div>Address: " + item.address + "</div>" +
      "<div>Distance to next stop: " + item.dist_next_stop + " meters</div>";
      var marker = new google.maps.Marker({
        position: item.loc,
        map: map,
        title: item.stop_id
      });

      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });

      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });

      markers.push(marker);
    });
  }


  function drawTripShape(shape) {
    var shapePath = [];

    shape.forEach(function(item) {
      shapePath.push(item.loc);
    });

    var path = new google.maps.Polyline({
      path: shapePath,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });

    path.setMap(map);
  }

  function drawStartAndEndMarks(start_point, end_point){
    var start_marker = new google.maps.Marker({
      position: start_point.loc,
      icon: "/assets/material-icons/ic_star_border_black_24px.svg",
      title: "Starting Point",
      map: map,
    });

    var end_marker = new google.maps.Marker({
      position: end_point.loc,
      icon: "/assets/material-icons/beenhere.svg",
      title: "End Stop",
      map: map,
    });
  }

  function drawTrip(url) {
    clearMarkers();
    $.ajax({
      dataType: "json",
      url: url,
    }).done(function(data) {
      console.log(data);
      var start_point = data.shape[0];
      var end_point = data.shape[data.shape.length - 1];
      drawTripShape(data.shape);
      drawStartAndEndMarks(start_point, end_point);
      drawStops(data.stops);
    });
  }

  function initMap() {
    var myLatLng = {lat: INIT_LAT, lng: INIT_LNG};

    map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: INIT_ZOOM
    });
  }

  initMap();
  drawTrip(received_url);
}
