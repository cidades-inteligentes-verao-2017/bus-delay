class SnapshotController < ApplicationController
  def show_ajax
    trip_id = params['trip_id']
    timestamp = Time.parse(params['timestamp']) unless params['timestamp'].nil?
    @bus_trips = BusTrip.snapshot_positions(trip_id, timestamp)
    render json: @bus_trips, status: 200
  end
  def show
    trip_id = params['trip_id']
    @trip = Trip.find_by(trip_id: trip_id)
  end
end
