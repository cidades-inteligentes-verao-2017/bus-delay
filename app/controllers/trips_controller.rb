class TripsController < ApplicationController
  def index
    if params[:search]
      @trips = Trip.search(params[:search]).asc(:route_id).page(params[:page]).per(50)
    else
      @trips = Trip.asc(:route_id).page(params[:page]).per(50)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    trip_id = params['trip_id']
    @trip = Trip.find_by(trip_id: trip_id)
    respond_to do |format|
      format.html
      format.json { render json: {
          trip_id: @trip.id,
          headsign: @trip.headsign,
          freq: @trip.freq,
          distance: @trip.shape_dist,
          shape: @trip.shape,
          stops: @trip.stops,
        },
        status: 200
      }
    end
  end
end
