class BusDelayController < ApplicationController
  def index
  end

  def traffic_delay
    travels = params["travels"] || 50
    limit = params["limit"] || 10
    @delays = BusInterval.mean_traffic_delay_by_trip(travels, limit)
    respond_to do |format|
      format.json {render :json => @delays, status: 200}
    end
  end

  def data_quality
    quality = {}
    DataQuality.all.each do |data|
     quality[data.name] = data.count
    end
    respond_to do |format|
      format.json {render :json => quality, status: 200}
    end
  end
  
  def stops
    stops = Stop.exists(statistics: true).pluck("loc.lat", "loc.lng", "statistics.avg")
    stops_array = []
    stops.each do |stop|
      next if stop[2]["avg"].nil?
      temp = []
      temp << stop[0]["lat"]
      temp << stop[1]["lng"]
      temp << stop[2]["avg"]
      stops_array << temp
    end
    respond_to do |format|
      format.json {render :json => stops_array, status: 200}
    end
  end
end
