class FreqDispatchController < ApplicationController
  
  def data
    trip_id = params['trip_id']
    freqs_dispatch = FreqDispatch.where(trip_id: trip_id)
    @freq = FreqDispatch.get_freq_info(freqs_dispatch)
    respond_to do |format|
      format.html
      format.json {render :json => @freq, status: 200}
    end
  end
end
