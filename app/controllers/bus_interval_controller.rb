class BusIntervalController < ApplicationController
  def index
    trip_id = params['trip_id']
    @trip = Trip.find_by(trip_id: trip_id)
  end

  def data
    trip_id = params['trip_id']
    bus_interval = BusInterval.where(trip_id: trip_id)
      .where(:"current_bus.travel_time".gte => 10.0)
      .where(:"next_bus.travel_time".gte => 10.0)
      .where(:"current_bus.travel_time".lte => 200.0)
      .where(:"next_bus.travel_time".lte => 200.0)
      .order_by("current_bus.begin_ts" => 'asc')
      .pluck("current_bus.begin_ts", :begin_interval, :end_interval, "current_bus.travel_time")
    mean_travel_time = BusTrip.mean_travel_time_by_hour(trip_id)
    @json = []
    bus_interval.each do |element|
      @json << [element[0]['begin_ts'], element[1], element[2], element[3]['travel_time'], element[2] - element[1], mean_travel_time[element[0][:begin_ts].hour]]
    end

    respond_to do |format|
      format.csv {send_data @bus_interval.to_csv}
      format.json {render :json => @json, status: 200}
    end
  end

  def trips
    trip_id = params['trip_id']
    trip = Trip.find_by(trip_id: trip_id)
    t1 = Time.parse("2014-10-21 17:00:00 UTC")
    t2 = Time.parse("2014-10-21 19:00:00 UTC")
    bus_trips = BusTrip.where(trip_id: trip_id, :begin_ts.gte => t1, :begin_ts.lte => t2).valid

    stops_map = {}
    json = {}
    trip.stops.each do |stop|
      stops_map[stop[:stop_id]] = stop[:stop_seq]
      json[stop[:stop_seq]] = []
    end

    i = 0
    bus_trips.each do |bus_trip|
      bus_trip["stops"]["list"].each do |stop|
        if stop && stop["stop_id"] && stop["ts_avl"]
          json[stops_map[stop["stop_id"]]] << stop["ts_avl"]
        end
      end
      i += 1
      json.keys.each do |key|
        if json[key].count < i
          json[key] << nil
        end
      end
    end

    json = json.to_a
    json.map!{|x| x.flatten}

    respond_to do |format|
      format.json {render :json => json, status: 200}
    end
  end

  def statistics
    trip_id = params['trip_id']
    @bus_interval = BusInterval.statistics(trip_id)

    respond_to do |format|
      format.csv {send_data @bus_interval.to_csv}
      format.json {render :json => @bus_interval, status: 200}
    end
  end
end
