# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170207125813) do

  create_table "frequencies", force: :cascade do |t|
    t.integer  "trip_id"
    t.time     "start_time"
    t.time     "end_time"
    t.integer  "interval_seconds"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["trip_id"], name: "index_frequencies_on_trip_id"
  end

  create_table "routes", force: :cascade do |t|
    t.string   "route_id"
    t.string   "short_name"
    t.string   "long_name"
    t.integer  "route_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shape_points", force: :cascade do |t|
    t.integer  "shape_id"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "sequence"
    t.float    "distance"
    t.integer  "trip_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["trip_id"], name: "index_shape_points_on_trip_id"
  end

  create_table "stop_times", force: :cascade do |t|
    t.integer  "trip_id"
    t.integer  "stop_id"
    t.time     "arrival_time"
    t.integer  "sequence"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["stop_id"], name: "index_stop_times_on_stop_id"
    t.index ["trip_id"], name: "index_stop_times_on_trip_id"
  end

  create_table "stops", force: :cascade do |t|
    t.integer  "stop_id"
    t.string   "name"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trips", force: :cascade do |t|
    t.string   "trip_id"
    t.integer  "route_id"
    t.string   "service_id"
    t.string   "headsign"
    t.integer  "direction_id"
    t.integer  "shape_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["route_id"], name: "index_trips_on_route_id"
  end

end
