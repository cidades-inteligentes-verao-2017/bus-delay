class CreateStopTimes < ActiveRecord::Migration[5.0]
  def change
    create_table :stop_times do |t|
      t.references :trip
      t.references :stop
      t.time :arrival_time
      t.integer :sequence
      t.timestamps
    end
  end
end
