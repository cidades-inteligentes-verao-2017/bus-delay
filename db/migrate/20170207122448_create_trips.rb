class CreateTrips < ActiveRecord::Migration[5.0]
  def change
    create_table :trips do |t|
      t.string :trip_id
      t.references :route
      t.string :service_id
      t.string :headsign
      t.integer :direction_id
      t.integer :shape_id
      t.timestamps
    end
  end
end
