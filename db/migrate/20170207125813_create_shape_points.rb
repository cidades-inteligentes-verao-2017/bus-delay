class CreateShapePoints < ActiveRecord::Migration[5.0]
  def change
    create_table :shape_points do |t|
      t.integer :shape_id
      t.float :latitude, scale: 10
      t.float :longitude, scale: 10
      t.integer :sequence
      t.float :distance
      t.references :trip
      t.timestamps
    end
  end
end
