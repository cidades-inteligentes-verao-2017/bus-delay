class CreateStops < ActiveRecord::Migration[5.0]
  def change
    create_table :stops do |t|
      t.integer :stop_id
      t.string :name
      t.float :latitude, scale: 10
      t.float :longitude, scale: 10
      t.timestamps
    end
  end
end
