class CreateFrequencies < ActiveRecord::Migration[5.0]
  def change
    create_table :frequencies do |t|
      t.references :trip
      t.time :start_time
      t.time :end_time
      t.integer :interval_seconds
      t.timestamps
    end
  end
end
