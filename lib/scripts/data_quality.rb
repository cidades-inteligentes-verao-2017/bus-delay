#!/usr/bin/env ruby

# Load Rails
ENV['RAILS_ENV'] = ENV['RAILS_ENV'] || 'development'

require File.expand_path("../../../config/environment", __FILE__)

module DataQualityCalculator
  def self.calculate
    puts "Calculating data quality"
    DataQuality.destroy_all
    completed = DataQuality.create!(name: "completed", count: BusTrip.where(status: "completed").count)
    partial = DataQuality.create!(name: "partial", count: BusTrip.where(status: "partial").count)
    reversed = DataQuality.create!(name: "reversed", count: BusTrip.where(status: "reversed").count)
    error = DataQuality.create!(name: "error", count: (BusTrip.count - completed.count - partial.count - reversed.count))
    puts "Finished!"
  end
end

DataQualityCalculator.calculate
