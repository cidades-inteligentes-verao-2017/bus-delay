#!/usr/bin/env ruby

require 'csv'

# Load Rails
ENV['RAILS_ENV'] = ENV['RAILS_ENV'] || 'development'

require File.expand_path("../../../config/environment", __FILE__)

module GTFS
  module Importer
    CSV_DIR = Rails.root.to_s + "/lib/scripts/files/"

    def self.routes
      puts "Importing Routes"

      CSV.foreach(CSV_DIR + 'routes.csv', headers: true) do |row|
        route = Route.new(
          route_id: row["route_id"],
          short_name: row["route_short_name"],
          long_name: row["route_long_name"],
          route_type: row["route_type"]
        )

        if route.save
          print '.'
        else
          print 'F'
        end
      end

      print "\n"
      puts "#{Route.count} routes created!"
    end

    def self.trips
      puts "Importing Trips"

      CSV.foreach(CSV_DIR + 'trips.csv', headers: true) do |row|
        route = Route.find_by_route_id(row["route_id"])
        if route.nil?
          print 'F'
          next
        end

        trip = Trip.new(
          trip_id: row["trip_id"],
          route_id: route.id,
          service_id: row["service_id"],
          headsign: row["trip_headsign"],
          direction_id: row["direction_id"],
          shape_id: row["shape_id"]
        )

        if trip.save
          print '.'
        else
          print 'F'
        end
      end

      print "\n"
      puts "#{Trip.count} trips created!"
    end

    def self.stops
      puts "Importing Stops"

      CSV.foreach(CSV_DIR + 'stops.csv', headers: true) do |row|
        stop = Stop.new(
          stop_id: row["stop_id"],
          name: row["stop_name"],
          latitude: row["stop_lat"],
          longitude: row["stop_lon"]
        )

        if stop.save
          print '.'
        else
          print 'F'
        end
      end

      print "\n"
      puts "#{Stop.count} stops created!"
    end

    def self.stop_times
      puts "Importing Stop_Times"

      CSV.foreach(CSV_DIR + 'stop_times.csv', headers: true) do |row|
        trip = Trip.find_by_trip_id(row["trip_id"])
        if trip.nil?
          print '1F'
          next
        end

        stop = Stop.find_by_stop_id(row["stop_id"])
        if stop.nil?
          print '2F'
          next
        end

        stop_time = StopTime.new(
          trip_id: trip.id,
          stop_id: stop.id,
          arrival_time: row["arrival_time"],
          sequence: row["stop_sequence"]
        )

        if stop_time.save
          print '.'
        else
          print 'F'
        end
      end

      print "\n"
      puts "#{StopTime.count} stop_times created!"
    end


    def self.shape_points
      puts "Importing ShapesPoints"

      CSV.foreach(CSV_DIR + 'shapes.csv', headers: true) do |row|

        trip = Trip.find_by_shape_id(row["shape_id"])
        if trip.nil?
          print 'F'
          next
        end

        shape_point = ShapePoint.new(
          shape_id: row["shape_id"],
          latitude: row["shape_pt_lat"],
          longitude: row["shape_pt_lon"],
          sequence: row["shape_pt_sequence"],
          distance: row["shape_dist_traveled"],
          trip_id: trip.id
        )

        if shape_point.save
          print '.'
        else
          print 'F'
        end
      end

      print "\n"
      puts "#{ShapePoint.count} shapes created!"
    end

    def self.frequencies
      puts "Importing Frequencies"

      CSV.foreach(CSV_DIR + 'frequencies.csv', headers: true) do |row|
        trip = Trip.find_by_trip_id(row["trip_id"])
        if trip.nil?
          print 'F'
          next
        end

        frequency = Frequency.new(
          start_time: row["start_time"],
          end_time: row["end_time"],
          interval_seconds: row["headway_secs"],
          trip_id: trip.id
        )

        if frequency.save
          print '.'
        else
          print 'F'
        end
      end

      print "\n"
      puts "#{Frequency.count} frenquencies created!"
    end
  end
end

#GTFS::Importer.routes
#GTFS::Importer.trips
#GTFS::Importer.stops
#GTFS::Importer.stop_times
#GTFS::Importer.shape_points
#GTFS::Importer.frequencies
