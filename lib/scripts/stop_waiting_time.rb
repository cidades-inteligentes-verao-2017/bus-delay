#!/usr/bin/env ruby

# Load Rails
ENV['RAILS_ENV'] = ENV['RAILS_ENV'] || 'development'

require File.expand_path("../../../config/environment", __FILE__)

module StopWaitingTime
  def self.update_stop_waiting_time
    puts "Updating average Stop Waiting Time"
    stops_data = FreqDispatch.stops_data
    stops_data.each do |data|
      stop = Stop.where(stop_id: data["_id"]).first
      if stop.blank?
        print "F[#{data["_id"]}]"
        next
      end
      stop["statistics"] = data.slice("avg", "sum", "max", "min", "count")
      if stop.save
        print '.'
      else
        print 'F'
      end
    end
    print "\n"
  end
end

StopWaitingTime.update_stop_waiting_time
