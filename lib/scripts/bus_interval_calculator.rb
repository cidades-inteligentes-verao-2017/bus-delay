#!/usr/bin/env ruby

require 'csv'

# Load Rails
ENV['RAILS_ENV'] = ENV['RAILS_ENV'] || 'development'

require File.expand_path("../../../config/environment", __FILE__)

module BusIntervalCalculator
  def self.update_interval_error
    puts "Updating interval time of Bus Trips"
    trip_ids = Trip.all.pluck(:trip_id).uniq
    #trip_ids = [
    #  '875C-10-1', '875C-10-0',
    #  '917H-10-0', '917H-10-1',
    #  '8012-10-0', '8012-10-1',
    #  '809U-10-1', '809U-10-0',
    #  '701U-10-1', '701U-10-0'
    #]

    trip_ids.each do |trip_id|
      bus_trips = BusTrip.where(trip_id: trip_id)
        .where(:begin_ts.nin => ["", nil], :end_ts.nin => ["", nil])
        .order_by(begin_ts: 'asc')

      current_bus = nil
      bus_trips.each do |next_bus|
        bus_interval = BusInterval.new(trip_id: trip_id)

        if current_bus.nil?
          current_bus = next_bus
          next
        end

        if current_bus.acceptable? && next_bus.acceptable?
          bus_interval.begin_interval = current_bus.begin_interval(next_bus)
          bus_interval.end_interval = current_bus.end_interval(next_bus)
          bus_interval.current_bus = {
            bus_id: current_bus.avl_id,
            begin_ts: current_bus.begin_ts,
            end_ts: current_bus.end_ts,
            travel_time: current_bus.travel_time
          }
          bus_interval.next_bus = {
            bus_id: next_bus.avl_id,
            begin_ts: next_bus.begin_ts,
            end_ts: next_bus.end_ts,
            travel_time: next_bus.travel_time
          }

          if bus_interval.save
            print '.'
          else
            print 'F'
          end
        end
        current_bus = next_bus
      end
    end
    print "\n"
  end
end

BusIntervalCalculator.update_interval_error
