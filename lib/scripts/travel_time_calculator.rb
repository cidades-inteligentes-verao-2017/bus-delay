#!/usr/bin/env ruby

require 'csv'

# Load Rails
ENV['RAILS_ENV'] = ENV['RAILS_ENV'] || 'development'

require File.expand_path("../../../config/environment", __FILE__)

module TravelTimeCalculator
  def self.update_travel_time
    puts "Updating travel time of Bus Trips"
    BusTrip.all.each do |bus_trip|
      bus_trip[:travel_time] = bus_trip.travel_time_in_minutes
      if bus_trip.save
        print '.'
      else
        print 'F'
      end
    end
    print "\n"
  end
end

TravelTimeCalculator.update_travel_time
