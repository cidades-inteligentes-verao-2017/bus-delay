# README

Bus Delay Web Application

## Setup

* Install Ruby 2.3.0 - we recommend you to install the Ruby environment with
[RVM](https://rvm.io/rvm/install)

* Install bundler: `gem install bundler`

* Clone the repository and install dependencies:
```
$ bundle install
```

## Database creation

* To restore and import data to mongo, run:
```
mongorestore dump-scipop
```
```
rake db:mongoid:create_indexes
```
```
ruby lib/scripts/travel_time_calculator.rb
```
```
ruby lib/scripts/bus_interval_calculator.rb
```
```
ruby lib/scripts/stop_waiting_time.rb
```
```
ruby lib/scripts/data_quality.rb
```


## Test

* To run tests:
```
$ rspec
```
**You should see all test passing =)**

## Deployment

*TODO*

## Mongo Access Examples

In MongoDB we have imported three collections from Scipopulis database:
* **trips** - trips information and their respective stops and shape data
* **stops** - information about bus stops
* **logs_bustrips** - this collection stores bus' travels with timestamps
information about:
  * first data sent
  * last data sent
  * departure
  * arrival
  * list of stops a bus passed through in a given trip

You can use the following examples to access most important data about Bus
trips in mongodb through Rails

* Get a random Trip (the same idea could be used in other models):
```
trip = Trip.first
trip
trip.trip_id
trip.stops
trip.stops.first[:loc]
```
* Querying by attributes
```
Stop.find_by(address: "R. D. Macário, 199")
# A where querying always returns a Mongoid::Criteria object. You should
# always iterate over it or access its elements:
BusTrip.where(trip_id: "875C-10-0")
BusTrip.where(trip_id: "875C-10-0").count
BusTrip.where(trip_id: "875C-10-0").first
```
* Getting a snapshot based on a timestamp and trip
```
t = Time.parse("2017-02-07 17:00:00 UTC")
BusTrip.snapshot_positions("875C-10-1", t)
```
